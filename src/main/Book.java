package main;

public class Book {
    private String titulo;
    private int numeroCopias;

    public Book(String titulo, int numeroCopias){
        this.titulo = titulo;
        this.numeroCopias = numeroCopias;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getNumeroCopias() {
        return numeroCopias;
    }

    public void setNumeroCopias(int numeroCopias) {
        this.numeroCopias = numeroCopias;
    }
}
