package main;

public class Calculadora {
    public int toCube(int numero){
        return numero * numero * numero;
    }
    public double potencia(double base, int exponente){
        double resultado = 1;
        for (int i = 0; i < exponente; i++){
            resultado *= base;
        }
        return resultado;
    }
}
