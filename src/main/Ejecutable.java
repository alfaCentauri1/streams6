package main;

import java.util.List;
import java.util.Locale;

public class Ejecutable {
    public static void main(String args[]) {
        System.out.println("Operaciones aritméticas con un Stream.");
        List<Integer> numeros = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        Long cantidad = numeros.stream().filter( numero -> numero > 6).count();
        System.out.println("La cantidad es " + cantidad);
        //Suma
        int suma = numeros.stream().mapToInt( n -> n).sum();
        System.out.println("La suma: " + suma);
        //Promedio
        double promedio = numeros.stream().mapToInt( n-> n ).average().orElse(0.0);
        System.out.println("El promedio: " + promedio);
        //Mínimo
        int minimo = numeros.stream().mapToInt( n-> n ).min().getAsInt();
        System.out.println("El minimo: " + minimo);
        //Máximo
        int maximo = numeros.stream().mapToInt( n-> n ).max().getAsInt();
        System.out.println("El máximo: " + maximo);
    }
}
