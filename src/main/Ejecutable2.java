package main;

import java.util.List;
import java.util.stream.Stream;

public class Ejecutable2 {
    public static void main(String args[]) {
        System.out.println("Operaciones aritméticas con un Stream.");
        List<Integer> numeros = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        int resultado = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10).reduce(0, (acumulador, elemento)-> acumulador + elemento );
        System.out.println(resultado);
    }
}
