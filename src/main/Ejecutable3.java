package main;

import java.util.List;

public class Ejecutable3 {
    public static void main(String args[]) {
        System.out.println("Operaciones con Streams y el metodo distinct.");
        List<String> names = List.of("Carlos", "Carlos", "Test 1", "Test 2", "Test 2");
        names.stream().distinct().forEach( elemento -> System.out.println(elemento) );
    }
}
