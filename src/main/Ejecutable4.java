package main;

import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Ejecutable4 {
    public static void main(String args[]){
        List<Curso> cursos = new ArrayList<>();
        cursos.add(new Curso("Cursos profesional de Java", 6.5f, 50, 200 ));
        cursos.add(new Curso("Cursos profesional de Python", 8.5f, 60, 800 ));
        cursos.add(new Curso("Cursos profesional de DB", 4.5f, 70, 700 ));
        cursos.add(new Curso("Cursos profesional de Android", 7.5f, 10, 400 ));
        cursos.add(new Curso("Cursos profesional de Escritura", 1.5f, 10, 300 ));
        //Obtener la cantidad de cursos con una duración mayor a 5 horas.
        int cantidadCursos = (int) cursos.stream().filter( curso -> curso.getDuracion() > 5 ).count();
        System.out.println("Cantidad de cursos con una duración mayor a 5 horas: " + cantidadCursos);
        //Obtener la cantidad de cursos con una duración menor a 2 horas
        cantidadCursos = (int) cursos.stream().filter( curso -> curso.getDuracion() < 2 ).count();
        System.out.println("Cantidad de cursos con una duración menor a 2 horas: " + cantidadCursos);
        //
        System.out.println("Listar el título de todos aquellos cursos con una cantidad de vídeos mayor a 50 ");
        cursos.stream().filter( curso -> curso.getVideos() > 50 )
                .forEach( elemento -> System.out.println(elemento.getTitulo()) );
        //
        System.out.println("Mostrar en consola el título de los 3 cursos con mayor duración.");
        Comparator<Curso> comparator = Comparator.comparing( curso -> curso.getDuracion() );
        cursos.stream().sorted( comparator.reversed() ).limit( 3 ).forEach( elemento -> System.out.println(elemento.getTitulo()) );
        //Mostrar en consola la duración total de todos los cursos.
        double suma = cursos.stream().mapToDouble( curso -> curso.getDuracion() ).sum();
        System.out.println("Mostrar en consola la duración total de todos los cursos: " + suma);
        //Promedio
        System.out.println("Mostrar en consola los cursos que superan el promedio de duración: ");
        double promedio = cursos.stream().mapToDouble( curso -> curso.getDuracion() ).average().orElse(0.0);
        System.out.println("El promedio de duración: " + promedio);
        cursos.stream().filter( curso -> curso.getDuracion() > promedio )
                .forEach( elemento -> System.out.println(elemento.getTitulo()) );
        //Mostrar en consola la duración de todos aquellos cursos que tengan una cantidad de alumnos inscritos menor a 500.
        System.out.println("Mostrar en consola la duración de todos aquellos cursos que tengan una cantidad de alumnos inscritos menor a 500.");
        cursos.stream().filter( curso -> curso.getAlumnos() < 500 )
                .forEach( elemento -> System.out.println(elemento.getTitulo()) );
        System.out.println("Obtener el curso con mayor duración.");
        cursos.stream().sorted( comparator.reversed() ).limit( 1 ).forEach( elemento -> System.out.println(elemento.getTitulo()) );
        System.out.println("Crear una lista de Strings con todos los titulos de los cursos.");
        List<String> lista = cursos.stream()
                .map( curso -> curso.getTitulo() )
                .collect(Collectors.toList());
        lista.forEach( elemento -> System.out.println(elemento) );
    }
}
