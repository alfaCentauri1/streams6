package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Ordenamiento {
    public static void main(String args[]) {
        System.out.println("Operaciones con Streams y el metodo de ordenamiento.");
        List<Book> books = new ArrayList<Book>();
        //Mostrar en la consola los tres titulos más vendidos.
        //Ordenar por copias
        books.add( new Book("El Señor de los Anillos", 1500) );
        books.add( new Book("Don Quijote de la Mancha", 15000) );
        books.add( new Book("Historia de dos ciudades", 200) );
        books.add( new Book("El Hobbit", 500) );
        books.add( new Book("El Principito", 140) );
        Comparator<Book> comparatorCopies = Comparator.comparing( book -> book.getNumeroCopias() );
        books.stream().sorted( comparatorCopies.reversed() )
                .limit(3)
                .forEach( book -> System.out.println( "* " + book.getTitulo() ) );
    }
}
