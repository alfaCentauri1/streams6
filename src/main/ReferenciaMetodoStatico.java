package main;

import java.util.List;

public class ReferenciaMetodoStatico {
    public static void main(String args[]) {
        System.out.println("Operaciones aritméticas con un Stream.");
        List<Integer> numeros = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println("Resultado: ");
        numeros.stream().map( valor -> ReferenciaMetodoStatico.toCube(valor) ).forEach(valor -> System.out.println(valor));
        //Otra forma
        System.out.println("Resultado por referencia: ");
        numeros.stream().map( ReferenciaMetodoStatico::toCube).forEach( System.out::println );
        //Por metodo de instancia
        Calculadora calculadora = new Calculadora();
        System.out.println("Resultado por metodo de instacia: ");
        numeros.stream().map( calculadora::toCube ).forEach( System.out::println );
        System.out.println("Resultado por metodo de instacia, para elevar a 4 los números: ");
        numeros.stream().map( base -> calculadora.potencia(base, 4)).forEach( System.out::println );
    }

    public static int toCube(int numero){
        return numero*numero*numero;
    }
}
